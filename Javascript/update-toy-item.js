document.getElementById("update-item-form").addEventListener("submit", updateItem);

function updateItem(e){
    e.preventDefault();
    const itemID = document.getElementById("item-id").value;
    const itemName = document.getElementById("item-name").value;
    //const itemPrice = document.getElementById("item-price").value;
    //const newItem = {"name": itemName, "price": itemPrice};
    // javascript object (newItem) -> json -> java object (MarketItem.class)
    ajaxUpdateItem(itemID, itemName, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "Item successfully update";
}

function indicateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue updating the item";
}