document.getElementById("new-item-form").addEventListener("submit", addNewItem);

function addNewItem(e){
    e.preventDefault();
    const itemName = document.getElementById("item-name").value;
    const itemPrice = document.getElementById("item-price").value;
    const newItem = {"toyName": itemName, "price": itemPrice};
    // javascript object (newItem) -> json -> java object (MarketItem.class)
    ajaxCreateItem(newItem, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "New item successfully created";
}

function indicateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue creating your new item";
}
