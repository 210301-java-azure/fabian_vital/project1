// getting item data from our server

const xhr = new XMLHttpRequest(); // ready state 0 
xhr.open("GET", "http://localhost:7000/items"); // ready state 1
// let token = sessionStorage.getItem("jwt"); // we could set the Auth header with this value instead
xhr.setRequestHeader("Authorization", "admin-auth-token");
xhr.onreadystatechange = function(){
    if(xhr.readyState == 4){
        if(xhr.status == 200){
            const items = JSON.parse(xhr.responseText);
            putItemsInTable(items);
            console.log(items);
        } else {
            console.log("something went wrong with your request")
        }
    }
}
xhr.send(); // ready state 2,3,4 follow 


function putItemsInTable(itemsList){
    document.getElementById("items-table").hidden = false;
    const tableBody = document.getElementById("items-table-body");
    for(let item of itemsList){
        let newRow = document.createElement("tr");
        newRow.innerHTML = `<td>${item.toyID}</td><td>${item.toyName}</td><td>$${item.price}</td>`;
        tableBody.appendChild(newRow);
    }
}

