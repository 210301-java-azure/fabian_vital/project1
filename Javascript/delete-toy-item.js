document.getElementById("delete-item-form").addEventListener("submit", deleteItem);

function deleteItem(e){
    e.preventDefault();
    const itemID = document.getElementById("item-id").value;
    // const newItem = {"name": itemName, "price": itemPrice};
    // javascript object (newItem) -> json -> java object (MarketItem.class)
    // ajaxCreateItem(newItem, indicateSuccess, indicateFailure);
    ajaxDeleteItem(itemID, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "Item successfully deleted";
}

function indicateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue deleting the item";
}