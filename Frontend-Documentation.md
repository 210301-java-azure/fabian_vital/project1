# Children's Favorite Toy Shop

![Toy Shop](kids-room-storage-.jpg)

## Overview
  A backend web service designed to provide data from a database to the client of a web application. Some of the technologies used are Java, Javalin, Hibernate, and Azure SQL Server. The purpose of the application is for the business owner of Children's Favorite Toy shop to keep track of the types of favorite toys for their frequent clients registered in their database and provide insight as to what types of toy items the owner can increase inventory in.

**Entity Relationship Diagram**

>![DataModel](ERD.PNG)


The above ERD shows the relationship between the entity sets stored in the Azure SQL Server database. A toy_items table that stores a list of toy items popular in toy shops around the country and a price for each. A second table, children, that stores childID, childName, age, gender, and their favorite toy item from the toy_items table.

---
A user can utilize the RESTful/Web Service to make each of the CRUD(creat, read, update, delete) sql command queries to the database tables.
It can handle four of the http methods:

- GET()
- POST()
- PUT()
- DELETE()

---

### GET/items
- The user is able to get all of the toy items in the toy_items table stored in the database.

>![Toys Table](getItems.jpg)

---

### POST/items
- The user is able to insert a new toy item record into the toy_items table stored in the database by explicity sending a JSON object in the body of the POST request.

>![Add Toys Form](addItems.jpg)

---

### PUT/items/id
- The user is able to update or edit a toy item using the toy item id in the toy_items table stored in the database.

>![Update Toys Form](updateItems.jpg)

---

### DELETE/items/id
- The user is able to delete a toy item from the toy_items table stored in the database by specifying and id number to delete.

>![Delete Toys Form](deleteItems.jpg)

## POST/Login
- Login is required to interact with the database as an administrator. An administrator alone can implement the CRUD SQL queries on the toy_items table; which mean he is able to add, delete, read, and update toy items in their inventory.

>>![Login Form](login.jpg)





